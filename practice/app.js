// var http = require('http');
// var fs = require('fs');

// var server = http.createServer(function (req, res) {
//     console.log('request was made:'+req.url);
//     if (req.url == '/home' || req.url == '/') { //check the URL of the current request
//             res.writeHead(200, { 'Content-Type': 'text/html' });
//             var readStream = fs.createReadStream(__dirname+'/index.html', 'utf8');
//             readStream.pipe(res)
//     }
//     else if(req.url == '/contact-us') {
//         res.writeHead(200, { 'Content-Type': 'text/html' });
//         var readStream = fs.createReadStream(__dirname+'/contact.html', 'utf8');
//         readStream.pipe(res)
//     }
//     else if (req.url == '/api/json') {
//       res.writeHead(200, { 'content-type': 'application/json'});
//       var myObj = {
//         name: 'Rohit Kumar Verma',
//         job: 'developer',
//         age: 23
//       }
//       res.end(JSON.stringify(myObj)) //we use stringify bcs end method take only string not json object
//     } else {
//       res.writeHead(200, { 'Content-Type': 'text/html' });
//       var readStream = fs.createReadStream(__dirname+'/404.html', 'utf8');
//       readStream.pipe(res)
//     }
// });

// server.listen(5000);

// console.log('Node.js web server at port 5000 is running..')

var express = require('express');
var app = express();
var ejs = require('ejs');


app.set('view engine', ejs);

app.get('/', function(req, res) {
  res.render('index.ejs');
})

app.get('/contact', function(req, res) {
  res.render('contact.ejs');
})

app.get('/about', function(req, res) {
  res.render('about.ejs');
})

// app.get('/profile/:id', function(req, res) {
//   res.send('you request to the profile with id '+ req.params.id);
// })

app.get('/profile/:name', function(req, res) {
  var data = {age: 23, job: 'web developer', hobbies: ['eating', 'playing', 'fishing', 'singing'] };
  res.render('profile.ejs', { person: req.params.name, data: data });
})


app.listen(3000);