var count = function(arr) {
  return 'there are '+arr.length + ' elements in the array'
}


var adder = function(a,b) {
  return `the sum of 2 num  id ${a+b}`
}

var pi = 3.14

module.exports= {count, adder}

module.exports.count = count
module.exports.adder = adder
module.exports.pi = pi
module.exports.random = 56

// OR

// module.exports = {
//   count: count,
//   adder: adder,
//   pi: pi
// }